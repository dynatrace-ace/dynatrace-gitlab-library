# Dynatrace Gitlab Library

This script library is used to easily integrate GitLab with Dynatrace.

## Version History

| Library Version | Comment |
| --------------- | ------- |
| v1.0 | Initial Release |

## Usage

First, include the `dynatrace_event.yaml` file from this repo into you `.gitlab-ci.yaml` file:
```yaml
include:
  - remote: 'https://gitlab.com/dynatrace-ace/dynatrace-gitlab-library/-/raw/master/dynatrace_event.yaml'
```
> Note: make sure to specify the correct release, as master is a working branch

Afterwards, you can start to include the event jobs to the right stages:

```yaml
dynatrace_info_event:
  extends: .dynatrace_event
  stage: send_event
  variables:
    DESCRIPTION: "An INFO event"
    CUSTOM_PROPERTIES: '{"label1":"foo","label2":"bar"}'
    EVENT_TYPE: "CUSTOM_INFO"
    TITLE: "My Event Title"
```

The following event types are supported:
- CUSTOM_INFO
- CUSTOM_ANNOTATION
- CUSTOM_DEPLOYMENT
- CUSTOM_CONFIGURATION

Please see https://www.dynatrace.com/support/help/dynatrace-api/environment-api/events/post-event/ for more details on the event types.

> Note: the job needs to run on a runner that has the following commands available:
> - yq
> - curl
> - jq

## Variables
This library uses a couple of variables that need to be set, either at job, pipeline or project level.


### Common Variables
The following variables are common accross all event types
| Variable | Description | Required | Default | 
| -------- | ----------- | -------- | ------- |
| DYNATRACE_ENV_URL | The endpoint of the Dynatrace environment. Format: https://my-managed.com/e/ENVIRONMENT_ID or https://env12345.live.dynatrace.com | **yes** | |
| DYNATRACE_API_TOKEN | The API token to authenticate with the Dynatrace API. Suggest to set this as a secret | **yes** | |
| EVENT_TYPE | The type of event you want to send | **yes** | CUSTOM_INFO |
| ATTACH_RULES_FILE | File that contains the yaml description on where to attach the event to. Check [samples/dynatrace.attachrules.yaml](samples/dynatrace.attachrules.yaml). You can alo leverage the `${ENV_VAR_NAME}` notation in the file which will be substituted by the environment variable value | **yes** | dynatrace/dynatrace.attachrules.yaml |
| CUSTOM_PROPERTIES | Additional properties to set on the event, using the format `{"label1":"foo","label2":"bar"}` | no | 

### Event-based variables
The following variables are specific for the event type. Please see https://www.dynatrace.com/support/help/dynatrace-api/environment-api/events/post-event/ for more details on the event types and fields.

| Variable | Description | CUSTOM_INFO | CUSTOM_ANNOTATION | CUSTOM_DEPLOYMENT | CUSTOM_CONFIGURATION |
| -------- | ----------- | ----------- | ----------------- | ----------------- | -------------------- |
| DESCRIPTION | Description of the event | *req* | *opt* | | *req* |
| TITLE | Title of the event | *opt* | | | |
| ANNOTATION_TYPE | | | *req* | | |
| ANNOTATION_DESCRIPTION | | | *req* | | |
| CONFIGURATION | The configuration that was changed | | | | *req* | 
| ORIGINAL | The original value of the configuration | | | | *opt* |
| DEPLOYMENT_NAME | | | | *req* | |
| DEPLOYMENT_VERSION | | | | *req* | |
| CI_BACK_LINK | | | | *opt* - defaults to job URL | |
| REMEDIATION_ACTION | | | | *opt* | |

## Support
This library was created by the Dynatrace ACE Innovation team. It is not supported by Dynatrace product support, please contact aceservices@dynatrace.com for more information.